﻿namespace Собираем_по_частям
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.baza = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bulka2 = new System.Windows.Forms.RadioButton();
            this.bulka4 = new System.Windows.Forms.RadioButton();
            this.bulka1 = new System.Windows.Forms.RadioButton();
            this.bulka3 = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.sous8 = new System.Windows.Forms.CheckBox();
            this.sous7 = new System.Windows.Forms.CheckBox();
            this.sous6 = new System.Windows.Forms.CheckBox();
            this.sous5 = new System.Windows.Forms.CheckBox();
            this.sous4 = new System.Windows.Forms.CheckBox();
            this.sous3 = new System.Windows.Forms.CheckBox();
            this.sous2 = new System.Windows.Forms.CheckBox();
            this.sous1 = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.gotovka3 = new System.Windows.Forms.RadioButton();
            this.gotovka2 = new System.Windows.Forms.RadioButton();
            this.gotovka1 = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.kolichestvo7 = new System.Windows.Forms.NumericUpDown();
            this.kolichestvo8 = new System.Windows.Forms.NumericUpDown();
            this.kolichestvo6 = new System.Windows.Forms.NumericUpDown();
            this.kolichestvo5 = new System.Windows.Forms.NumericUpDown();
            this.kolichestvo4 = new System.Windows.Forms.NumericUpDown();
            this.kolichestvo3 = new System.Windows.Forms.NumericUpDown();
            this.kolichestvo2 = new System.Windows.Forms.NumericUpDown();
            this.kolichestvo1 = new System.Windows.Forms.NumericUpDown();
            this.ingridient8 = new System.Windows.Forms.CheckBox();
            this.ingridient7 = new System.Windows.Forms.CheckBox();
            this.ingridient6 = new System.Windows.Forms.CheckBox();
            this.ingridient5 = new System.Windows.Forms.CheckBox();
            this.ingridient4 = new System.Windows.Forms.CheckBox();
            this.ingridient3 = new System.Windows.Forms.CheckBox();
            this.ingridient2 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.res = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(283, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(358, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Добро пожаловать в World of Burgers";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(16, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(225, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Базовая стоимость бургера ";
            // 
            // baza
            // 
            this.baza.Enabled = false;
            this.baza.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.baza.Location = new System.Drawing.Point(19, 76);
            this.baza.Name = "baza";
            this.baza.Size = new System.Drawing.Size(201, 26);
            this.baza.TabIndex = 2;
            this.baza.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.groupBox2.Controls.Add(this.bulka2);
            this.groupBox2.Controls.Add(this.bulka4);
            this.groupBox2.Controls.Add(this.bulka1);
            this.groupBox2.Controls.Add(this.bulka3);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(286, 120);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(254, 136);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Тип булки";
            // 
            // bulka2
            // 
            this.bulka2.AutoSize = true;
            this.bulka2.Location = new System.Drawing.Point(16, 49);
            this.bulka2.Name = "bulka2";
            this.bulka2.Size = new System.Drawing.Size(212, 21);
            this.bulka2.TabIndex = 1;
            this.bulka2.TabStop = true;
            this.bulka2.Tag = "10";
            this.bulka2.Text = "Белая без кунжута (10 руб.)";
            this.bulka2.UseVisualStyleBackColor = true;
            // 
            // bulka4
            // 
            this.bulka4.AutoSize = true;
            this.bulka4.Location = new System.Drawing.Point(16, 103);
            this.bulka4.Name = "bulka4";
            this.bulka4.Size = new System.Drawing.Size(221, 21);
            this.bulka4.TabIndex = 1;
            this.bulka4.TabStop = true;
            this.bulka4.Tag = "12";
            this.bulka4.Text = "Чёрная без кунжута (12 руб.)";
            this.bulka4.UseVisualStyleBackColor = true;
            // 
            // bulka1
            // 
            this.bulka1.AutoSize = true;
            this.bulka1.Location = new System.Drawing.Point(16, 22);
            this.bulka1.Name = "bulka1";
            this.bulka1.Size = new System.Drawing.Size(205, 21);
            this.bulka1.TabIndex = 0;
            this.bulka1.TabStop = true;
            this.bulka1.Tag = "11";
            this.bulka1.Text = "Белая с кунжутом (11 руб.)";
            this.bulka1.UseVisualStyleBackColor = true;
            // 
            // bulka3
            // 
            this.bulka3.AutoSize = true;
            this.bulka3.Location = new System.Drawing.Point(16, 76);
            this.bulka3.Name = "bulka3";
            this.bulka3.Size = new System.Drawing.Size(214, 21);
            this.bulka3.TabIndex = 0;
            this.bulka3.TabStop = true;
            this.bulka3.Tag = "13";
            this.bulka3.Text = "Чёрная с кунжутом (13 руб.)";
            this.bulka3.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.groupBox3.Controls.Add(this.sous8);
            this.groupBox3.Controls.Add(this.sous7);
            this.groupBox3.Controls.Add(this.sous6);
            this.groupBox3.Controls.Add(this.sous5);
            this.groupBox3.Controls.Add(this.sous4);
            this.groupBox3.Controls.Add(this.sous3);
            this.groupBox3.Controls.Add(this.sous2);
            this.groupBox3.Controls.Add(this.sous1);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(546, 120);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(235, 255);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Соус";
            // 
            // sous8
            // 
            this.sous8.AutoSize = true;
            this.sous8.Location = new System.Drawing.Point(18, 218);
            this.sous8.Name = "sous8";
            this.sous8.Size = new System.Drawing.Size(172, 21);
            this.sous8.TabIndex = 7;
            this.sous8.Tag = "15";
            this.sous8.Text = "Соус Цезарь (15 руб.)";
            this.sous8.UseVisualStyleBackColor = true;
            // 
            // sous7
            // 
            this.sous7.AutoSize = true;
            this.sous7.Location = new System.Drawing.Point(18, 191);
            this.sous7.Name = "sous7";
            this.sous7.Size = new System.Drawing.Size(150, 21);
            this.sous7.TabIndex = 6;
            this.sous7.Tag = "20";
            this.sous7.Text = "Кетчунез (20 руб.)";
            this.sous7.UseVisualStyleBackColor = true;
            // 
            // sous6
            // 
            this.sous6.AutoSize = true;
            this.sous6.Location = new System.Drawing.Point(18, 163);
            this.sous6.Name = "sous6";
            this.sous6.Size = new System.Drawing.Size(141, 21);
            this.sous6.TabIndex = 5;
            this.sous6.Tag = "15";
            this.sous6.Text = "Сырный (15 руб.)";
            this.sous6.UseVisualStyleBackColor = true;
            // 
            // sous5
            // 
            this.sous5.AutoSize = true;
            this.sous5.Location = new System.Drawing.Point(18, 135);
            this.sous5.Name = "sous5";
            this.sous5.Size = new System.Drawing.Size(163, 21);
            this.sous5.TabIndex = 4;
            this.sous5.Tag = "15";
            this.sous5.Text = "Чесночный (15 руб.)";
            this.sous5.UseVisualStyleBackColor = true;
            // 
            // sous4
            // 
            this.sous4.AutoSize = true;
            this.sous4.Location = new System.Drawing.Point(18, 106);
            this.sous4.Name = "sous4";
            this.sous4.Size = new System.Drawing.Size(187, 21);
            this.sous4.TabIndex = 3;
            this.sous4.Tag = "15";
            this.sous4.Text = "Кисло-сладкий (15 руб.)";
            this.sous4.UseVisualStyleBackColor = true;
            // 
            // sous3
            // 
            this.sous3.AutoSize = true;
            this.sous3.Location = new System.Drawing.Point(18, 79);
            this.sous3.Name = "sous3";
            this.sous3.Size = new System.Drawing.Size(153, 21);
            this.sous3.TabIndex = 2;
            this.sous3.Tag = "15";
            this.sous3.Text = "Барбекью (15 руб.)";
            this.sous3.UseVisualStyleBackColor = true;
            // 
            // sous2
            // 
            this.sous2.AutoSize = true;
            this.sous2.Location = new System.Drawing.Point(18, 53);
            this.sous2.Name = "sous2";
            this.sous2.Size = new System.Drawing.Size(144, 21);
            this.sous2.TabIndex = 1;
            this.sous2.Tag = "15";
            this.sous2.Text = "Терияки (15 руб.)";
            this.sous2.UseVisualStyleBackColor = true;
            // 
            // sous1
            // 
            this.sous1.AutoSize = true;
            this.sous1.Location = new System.Drawing.Point(18, 26);
            this.sous1.Name = "sous1";
            this.sous1.Size = new System.Drawing.Size(204, 21);
            this.sous1.TabIndex = 0;
            this.sous1.Tag = "15";
            this.sous1.Text = "Кетчуп томатный (15 руб.)";
            this.sous1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.groupBox4.Controls.Add(this.gotovka3);
            this.groupBox4.Controls.Add(this.gotovka2);
            this.groupBox4.Controls.Add(this.gotovka1);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox4.Location = new System.Drawing.Point(286, 262);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(254, 113);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Способ приготовления котлеты";
            // 
            // gotovka3
            // 
            this.gotovka3.AutoSize = true;
            this.gotovka3.Location = new System.Drawing.Point(16, 78);
            this.gotovka3.Name = "gotovka3";
            this.gotovka3.Size = new System.Drawing.Size(170, 21);
            this.gotovka3.TabIndex = 4;
            this.gotovka3.TabStop = true;
            this.gotovka3.Tag = "4";
            this.gotovka3.Text = "На сковороде (4 руб.)";
            this.gotovka3.UseVisualStyleBackColor = true;
            // 
            // gotovka2
            // 
            this.gotovka2.AutoSize = true;
            this.gotovka2.Location = new System.Drawing.Point(16, 50);
            this.gotovka2.Name = "gotovka2";
            this.gotovka2.Size = new System.Drawing.Size(143, 21);
            this.gotovka2.TabIndex = 3;
            this.gotovka2.TabStop = true;
            this.gotovka2.Tag = "7";
            this.gotovka2.Text = "В духовке (7 руб.)";
            this.gotovka2.UseVisualStyleBackColor = true;
            // 
            // gotovka1
            // 
            this.gotovka1.AutoSize = true;
            this.gotovka1.Location = new System.Drawing.Point(16, 22);
            this.gotovka1.Name = "gotovka1";
            this.gotovka1.Size = new System.Drawing.Size(138, 21);
            this.gotovka1.TabIndex = 2;
            this.gotovka1.TabStop = true;
            this.gotovka1.Tag = "10";
            this.gotovka1.Text = "На огне (10 руб.)";
            this.gotovka1.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.kolichestvo7);
            this.groupBox5.Controls.Add(this.kolichestvo8);
            this.groupBox5.Controls.Add(this.kolichestvo6);
            this.groupBox5.Controls.Add(this.kolichestvo5);
            this.groupBox5.Controls.Add(this.kolichestvo4);
            this.groupBox5.Controls.Add(this.kolichestvo3);
            this.groupBox5.Controls.Add(this.kolichestvo2);
            this.groupBox5.Controls.Add(this.kolichestvo1);
            this.groupBox5.Controls.Add(this.ingridient8);
            this.groupBox5.Controls.Add(this.ingridient7);
            this.groupBox5.Controls.Add(this.ingridient6);
            this.groupBox5.Controls.Add(this.ingridient5);
            this.groupBox5.Controls.Add(this.ingridient4);
            this.groupBox5.Controls.Add(this.ingridient3);
            this.groupBox5.Controls.Add(this.ingridient2);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox5.Location = new System.Drawing.Point(19, 120);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(261, 255);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Ингридиенты";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 17);
            this.label4.TabIndex = 16;
            this.label4.Text = "Котлета (25 руб.)";
            // 
            // kolichestvo7
            // 
            this.kolichestvo7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.kolichestvo7.Location = new System.Drawing.Point(208, 189);
            this.kolichestvo7.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.kolichestvo7.Name = "kolichestvo7";
            this.kolichestvo7.Size = new System.Drawing.Size(39, 23);
            this.kolichestvo7.TabIndex = 14;
            this.kolichestvo7.Tag = "7";
            this.kolichestvo7.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kolichestvo7.Visible = false;
            // 
            // kolichestvo8
            // 
            this.kolichestvo8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.kolichestvo8.Location = new System.Drawing.Point(208, 218);
            this.kolichestvo8.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.kolichestvo8.Name = "kolichestvo8";
            this.kolichestvo8.Size = new System.Drawing.Size(39, 23);
            this.kolichestvo8.TabIndex = 15;
            this.kolichestvo8.Tag = "10";
            this.kolichestvo8.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kolichestvo8.Visible = false;
            // 
            // kolichestvo6
            // 
            this.kolichestvo6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.kolichestvo6.Location = new System.Drawing.Point(208, 161);
            this.kolichestvo6.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.kolichestvo6.Name = "kolichestvo6";
            this.kolichestvo6.Size = new System.Drawing.Size(39, 23);
            this.kolichestvo6.TabIndex = 13;
            this.kolichestvo6.Tag = "8";
            this.kolichestvo6.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kolichestvo6.Visible = false;
            // 
            // kolichestvo5
            // 
            this.kolichestvo5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.kolichestvo5.Location = new System.Drawing.Point(208, 132);
            this.kolichestvo5.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.kolichestvo5.Name = "kolichestvo5";
            this.kolichestvo5.Size = new System.Drawing.Size(39, 23);
            this.kolichestvo5.TabIndex = 12;
            this.kolichestvo5.Tag = "6";
            this.kolichestvo5.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kolichestvo5.Visible = false;
            // 
            // kolichestvo4
            // 
            this.kolichestvo4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.kolichestvo4.Location = new System.Drawing.Point(208, 104);
            this.kolichestvo4.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.kolichestvo4.Name = "kolichestvo4";
            this.kolichestvo4.Size = new System.Drawing.Size(39, 23);
            this.kolichestvo4.TabIndex = 11;
            this.kolichestvo4.Tag = "10";
            this.kolichestvo4.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kolichestvo4.Visible = false;
            // 
            // kolichestvo3
            // 
            this.kolichestvo3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.kolichestvo3.Location = new System.Drawing.Point(208, 77);
            this.kolichestvo3.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.kolichestvo3.Name = "kolichestvo3";
            this.kolichestvo3.Size = new System.Drawing.Size(39, 23);
            this.kolichestvo3.TabIndex = 10;
            this.kolichestvo3.Tag = "15";
            this.kolichestvo3.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kolichestvo3.Visible = false;
            // 
            // kolichestvo2
            // 
            this.kolichestvo2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.kolichestvo2.Location = new System.Drawing.Point(208, 50);
            this.kolichestvo2.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.kolichestvo2.Name = "kolichestvo2";
            this.kolichestvo2.Size = new System.Drawing.Size(39, 23);
            this.kolichestvo2.TabIndex = 9;
            this.kolichestvo2.Tag = "10";
            this.kolichestvo2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kolichestvo2.Visible = false;
            // 
            // kolichestvo1
            // 
            this.kolichestvo1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.kolichestvo1.Location = new System.Drawing.Point(208, 23);
            this.kolichestvo1.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.kolichestvo1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kolichestvo1.Name = "kolichestvo1";
            this.kolichestvo1.Size = new System.Drawing.Size(39, 23);
            this.kolichestvo1.TabIndex = 8;
            this.kolichestvo1.Tag = "25";
            this.kolichestvo1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ingridient8
            // 
            this.ingridient8.AutoSize = true;
            this.ingridient8.Location = new System.Drawing.Point(19, 220);
            this.ingridient8.Name = "ingridient8";
            this.ingridient8.Size = new System.Drawing.Size(185, 21);
            this.ingridient8.TabIndex = 7;
            this.ingridient8.Tag = "10";
            this.ingridient8.Text = "Жаренный лук (10 руб.)";
            this.ingridient8.UseVisualStyleBackColor = true;
            this.ingridient8.CheckedChanged += new System.EventHandler(this.Ingridient2_CheckedChanged);
            // 
            // ingridient7
            // 
            this.ingridient7.AutoSize = true;
            this.ingridient7.Location = new System.Drawing.Point(19, 191);
            this.ingridient7.Name = "ingridient7";
            this.ingridient7.Size = new System.Drawing.Size(104, 21);
            this.ingridient7.TabIndex = 6;
            this.ingridient7.Tag = "7";
            this.ingridient7.Text = "Лук (7 руб.)";
            this.ingridient7.UseVisualStyleBackColor = true;
            this.ingridient7.CheckedChanged += new System.EventHandler(this.Ingridient2_CheckedChanged);
            // 
            // ingridient6
            // 
            this.ingridient6.AutoSize = true;
            this.ingridient6.Location = new System.Drawing.Point(19, 163);
            this.ingridient6.Name = "ingridient6";
            this.ingridient6.Size = new System.Drawing.Size(187, 21);
            this.ingridient6.TabIndex = 5;
            this.ingridient6.Tag = "8";
            this.ingridient6.Text = "Солёный огурец (8 руб.)";
            this.ingridient6.UseVisualStyleBackColor = true;
            this.ingridient6.CheckedChanged += new System.EventHandler(this.Ingridient2_CheckedChanged);
            // 
            // ingridient5
            // 
            this.ingridient5.AutoSize = true;
            this.ingridient5.Location = new System.Drawing.Point(19, 135);
            this.ingridient5.Name = "ingridient5";
            this.ingridient5.Size = new System.Drawing.Size(120, 21);
            this.ingridient5.TabIndex = 4;
            this.ingridient5.Tag = "6";
            this.ingridient5.Text = "Салат (6 руб.)";
            this.ingridient5.UseVisualStyleBackColor = true;
            this.ingridient5.CheckedChanged += new System.EventHandler(this.Ingridient2_CheckedChanged);
            // 
            // ingridient4
            // 
            this.ingridient4.AutoSize = true;
            this.ingridient4.Location = new System.Drawing.Point(19, 107);
            this.ingridient4.Name = "ingridient4";
            this.ingridient4.Size = new System.Drawing.Size(147, 21);
            this.ingridient4.TabIndex = 3;
            this.ingridient4.Tag = "10";
            this.ingridient4.Text = "Помидор (10 руб.)";
            this.ingridient4.UseVisualStyleBackColor = true;
            this.ingridient4.CheckedChanged += new System.EventHandler(this.Ingridient2_CheckedChanged);
            // 
            // ingridient3
            // 
            this.ingridient3.AutoSize = true;
            this.ingridient3.Location = new System.Drawing.Point(19, 79);
            this.ingridient3.Name = "ingridient3";
            this.ingridient3.Size = new System.Drawing.Size(128, 21);
            this.ingridient3.TabIndex = 2;
            this.ingridient3.Tag = "15";
            this.ingridient3.Text = "Бекон (15 руб.)";
            this.ingridient3.UseVisualStyleBackColor = true;
            this.ingridient3.CheckedChanged += new System.EventHandler(this.Ingridient2_CheckedChanged);
            // 
            // ingridient2
            // 
            this.ingridient2.AutoSize = true;
            this.ingridient2.Location = new System.Drawing.Point(19, 51);
            this.ingridient2.Name = "ingridient2";
            this.ingridient2.Size = new System.Drawing.Size(115, 21);
            this.ingridient2.TabIndex = 1;
            this.ingridient2.Tag = "10";
            this.ingridient2.Text = "Сыр (10 руб.)";
            this.ingridient2.UseVisualStyleBackColor = true;
            this.ingridient2.CheckedChanged += new System.EventHandler(this.Ingridient2_CheckedChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(19, 389);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(261, 26);
            this.button1.TabIndex = 8;
            this.button1.Text = "Рассчитать";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(546, 389);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(235, 26);
            this.button2.TabIndex = 9;
            this.button2.Text = "Отчистить";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // res
            // 
            this.res.Enabled = false;
            this.res.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.res.Location = new System.Drawing.Point(286, 389);
            this.res.Name = "res";
            this.res.Size = new System.Drawing.Size(217, 26);
            this.res.TabIndex = 10;
            this.res.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(506, 393);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "руб.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label5.Location = new System.Drawing.Point(223, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "руб.";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Собираем_по_частям.Properties.Resources.burger6;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(655, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(114, 108);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AcceptButton = this.button1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(810, 429);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.res);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.baza);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "World of Burgers";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kolichestvo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox baza;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton bulka2;
        private System.Windows.Forms.RadioButton bulka1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton gotovka1;
        private System.Windows.Forms.RadioButton bulka4;
        private System.Windows.Forms.RadioButton bulka3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox sous8;
        private System.Windows.Forms.CheckBox sous7;
        private System.Windows.Forms.CheckBox sous6;
        private System.Windows.Forms.CheckBox sous5;
        private System.Windows.Forms.CheckBox sous4;
        private System.Windows.Forms.CheckBox sous3;
        private System.Windows.Forms.CheckBox sous2;
        private System.Windows.Forms.CheckBox sous1;
        private System.Windows.Forms.RadioButton gotovka3;
        private System.Windows.Forms.RadioButton gotovka2;
        private System.Windows.Forms.NumericUpDown kolichestvo7;
        private System.Windows.Forms.NumericUpDown kolichestvo8;
        private System.Windows.Forms.NumericUpDown kolichestvo6;
        private System.Windows.Forms.NumericUpDown kolichestvo5;
        private System.Windows.Forms.NumericUpDown kolichestvo4;
        private System.Windows.Forms.NumericUpDown kolichestvo3;
        private System.Windows.Forms.NumericUpDown kolichestvo2;
        private System.Windows.Forms.NumericUpDown kolichestvo1;
        private System.Windows.Forms.CheckBox ingridient8;
        private System.Windows.Forms.CheckBox ingridient7;
        private System.Windows.Forms.CheckBox ingridient6;
        private System.Windows.Forms.CheckBox ingridient5;
        private System.Windows.Forms.CheckBox ingridient4;
        private System.Windows.Forms.CheckBox ingridient3;
        private System.Windows.Forms.CheckBox ingridient2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox res;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

