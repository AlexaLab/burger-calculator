﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Собираем_по_частям
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            baza.Text = "20";            
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            res.Clear();

            if (bulka1.Checked == false && bulka2.Checked == false && bulka3.Checked == false && bulka4.Checked == false || gotovka1.Checked == false && gotovka2.Checked == false && gotovka3.Checked == false)
                return;

            double cena=20;

            //Прибавляем цены за булку
            if (bulka1.Checked)
                cena += 11;
            else if (bulka2.Checked)
                cena += 10;
            else if (bulka3.Checked)
                cena += 13;
            else if (bulka4.Checked)
                cena += 12;

            //Прибавляем цену за приготовление
            if (gotovka1.Checked)
                cena += 10;
            else if (gotovka2.Checked)
                cena += 7;
            else if (gotovka2.Checked)
                cena += 4;

            //Прибавляем цену за соус
            if (sous1.Checked)
                cena += 15;
            if (sous2.Checked)
                cena += 15;
            if (sous3.Checked)
                cena += 15;
            if (sous4.Checked)
                cena += 15;
            if (sous5.Checked)
                cena += 15;
            if (sous6.Checked)
                cena += 15;
            if (sous7.Checked)
                cena += 20;
            if (sous8.Checked)
                cena += 15;

            //Прибавляем цену за ингридиенты
            cena += (Convert.ToDouble(kolichestvo1.Value))*25;
            if (ingridient2.Checked)
                cena += (Convert.ToDouble(kolichestvo2.Value))*10;
            if (ingridient3.Checked)
                cena += (Convert.ToDouble(kolichestvo3.Value))*15;
            if (ingridient4.Checked)
                cena += (Convert.ToDouble(kolichestvo4.Value))*10;
            if (ingridient5.Checked)
                cena += (Convert.ToDouble(kolichestvo5.Value))*6;
            if (ingridient6.Checked)
                cena += (Convert.ToDouble(kolichestvo6.Value))*8;
            if (ingridient7.Checked)
                cena += (Convert.ToDouble(kolichestvo7.Value))*7;
            if (ingridient8.Checked)
                cena += (Convert.ToDouble(kolichestvo8.Value))*10;

            res.Text = cena.ToString();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            res.Clear();

            sous1.Checked = false;
            sous2.Checked = false;
            sous3.Checked = false;
            sous4.Checked = false;
            sous5.Checked = false;
            sous6.Checked = false;
            sous7.Checked = false;
            sous8.Checked = false;

            bulka1.Checked = false;
            bulka2.Checked = false;
            bulka3.Checked = false;
            bulka4.Checked = false;

            gotovka1.Checked = false;
            gotovka2.Checked = false;
            gotovka3.Checked = false;

            ingridient2.Checked = false;
            ingridient3.Checked = false;
            ingridient4.Checked = false;
            ingridient5.Checked = false;
            ingridient6.Checked = false;
            ingridient7.Checked = false;
            ingridient8.Checked = false;

            kolichestvo1.Value = 1;
            kolichestvo2.Value = 1;
            kolichestvo3.Value = 1;
            kolichestvo4.Value = 1;
            kolichestvo5.Value = 1;
            kolichestvo6.Value = 1;
            kolichestvo7.Value = 1;
            kolichestvo8.Value = 1;
        }

        private void Ingridient2_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ch = sender as CheckBox;
            string s = ch.Name, sn;
            char c1 = s[s.Length - 1], c2;
            NumericUpDown nud = null;

            for (int i = 0; i < this.groupBox5.Controls.Count; i++)
            {
                if (this.groupBox5.Controls[i] is NumericUpDown)
                {
                    nud = this.groupBox5.Controls[i] as NumericUpDown;
                    sn = nud.Name;
                    c2 = sn[sn.Length - 1];
                    if (c1 == c2)
                       break;
                 }
            }
             nud.Visible = ch.Checked;
        }
        //private void Ingridient5_CheckedChanged(object sender, EventArgs e)
        //{
        //    kolichestvo5.Visible = true;
        //    if (ingridient5.Checked == false)
        //        kolichestvo5.Visible = false;
        //}

        //private void Ingridient6_CheckedChanged(object sender, EventArgs e)
        //{
        //    kolichestvo6.Visible = true;
        //    if (ingridient6.Checked == false)
        //        kolichestvo6.Visible = false;
        //}

        //private void Ingridient7_CheckedChanged(object sender, EventArgs e)
        //{
        //    kolichestvo7.Visible = true;
        //    if (ingridient7.Checked == false)
        //        kolichestvo7.Visible = false;
        //}

        //private void Ingridient8_CheckedChanged(object sender, EventArgs e)
        //{
        //    kolichestvo8.Visible = true;
        //    if (ingridient8.Checked == false)
        //        kolichestvo8.Visible = false;
        //}
    }
}
